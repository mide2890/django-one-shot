from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoListItemForm

# Create your views here.
def show_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "items": todo_lists,
    }
    return render(request, "todos/list.html", context)


def show_detail(request, id):
    model_instance = TodoList.objects.get(id=id)
    context ={
        "items": model_instance,
    }
    return render(request, "todos/detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            model_instance = form.save(commit=False)
            model_instance.user = request.user
            model_instance.save()
            return redirect("show_detail", id=model_instance.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def edit_todo(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
        return redirect("show_detail", id=id)

    else:
        form = TodoListForm(instance=list)

    context = {
        "list_object": list,
        "todo_list_form": form,
    }

    return render(request, "todos/edit.html", context)


def delete_todo(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("show_list")

    return render(request, "todos/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = TodoListItemForm(request.POST)
        if form.is_valid():
            model_instance = form.save(commit=False)
            model_instance.user = request.user
            model_instance.save()
            return redirect("show_detail", id=model_instance.id)
    else:
        form = TodoListItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)


def edit_item (request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoListItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
        return redirect("show_detail", item.list.id)

    else:
        form = TodoListItemForm(instance=item)

    context = {
        "list_object": item,
        "todo_list_form": form,
    }

    return render(request, "todos/edit_item.html", context)
