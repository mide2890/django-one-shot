from django.urls import path
from todos.views import show_list, show_detail, create_todo, edit_todo, delete_todo, create_todo_item, edit_item

urlpatterns = [
    path("", show_list, name="show_list"),
    path("todos/<int:id>/", show_detail, name="show_detail"),
    path("create/", create_todo, name="create_todo"),
    path("<int:id>/edit/", edit_todo, name="edit_todo"),
    path("<int:id>/delete/", delete_todo, name="delete_todo"),
    path("create_item/", create_todo_item, name="create_todo_item"),
    path("<int:id>/edit_item/", edit_item, name="edit_item"),

]
